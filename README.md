<p>DiQuick Web UI框架，采用HTML+CSS3+原生JS，配置有响应式布局及预定义设置，包含诸多组件：Nav、Tab、Media、Form、Menu、Slider、Dialog等，致力于开发轻量化、语义化、扩展性强的Web UI框架。</p>

<p>软件中文首页<br />
<a href="http://www.diquick.com/" target="_blank">http://www.diquick.com/</a></p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0314/125228_81baf6cf_1073834.jpeg" /></p>

<h1>简化的HTML结构与预定义CSS</h1>

<p>预定义可以快速部署组件及CSS样式行为，在HTML文档中减少对子级的设定，更有效的简化了文档</p>

<p>我们不赞成对子级逐项定义，因为在调整时每一个子级class都成了负担，推荐使用预定义的class直接定义父级来控制整体效果，当然我们也提供了自定义的响应式布局</p>
<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0302/220112_39db8ad2_1073834.jpeg" /></p>

<h1>快速创建响应式布局</h1>

<p>响应式控制也将变的简单，class=&quot;list2 list-s1&quot;代表了列表在小屏幕时由每行2个转换为每行1个</p>

<p>在其他可减少子级逐项定义的组件中也采用了类似的设计</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0302/220117_f01facde_1073834.jpeg" /></p>

<h1>轻松创建私有样式</h1>

<p>我们提供了自定义风格工具以为你创建私有样式</p>

<p>在此过程中你不必了解框架组件原先的CSS是如何设定的，只需具有简单的HTML+CSS基础即可操作，以降低你的学习成本</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0302/220112_bd1f071f_1073834.gif" /></p>

<h1>外部加载的组件自动绑定事件</h1>

<p>在以往的设计中，脚本仅绑定文档中已存在的组件，新版本将支持自动遍厉外部加载的组件并实现事件绑定，告别手动初始化</p>

<p>你可以任意使用innerHTML、insertAdjacentHTML等方法来作前端渲染</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0302/220113_d2a40660_1073834.gif" /></p>
